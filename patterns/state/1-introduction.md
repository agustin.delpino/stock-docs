---
author: Del Pino, Agustin y Ortiz, Alan
---
# State Machine Pattern

The state machine pattern pretends to perform a big task in little steps 
which are called states. Each state will perform a small portion of the big task. 
An advantage of this pattern is to resume the state machine at any state.

# State Machine

The ***state machine*** is the orchestrator of the states . It's divided into three steps.
1. Pre-state
2. State Flow Execution
3. Error Handler State 

## The Pre-state
The pre-state, as its name says, it's the step before the execution of the states. 
It has the responsibility of indicate in which state the machine has to start. 
This step allows to the machine to resume from one specific state.

## The State Flow Execution
After the pre-state, the states are executed in the given order starting at 
the state determinate by the pre-state. 

## Error Handler State
This is an especial state which is used for handle any error occurred at any state.
This step allows to react to any error and it's possible to indicate 
the machine what to do next.

# State
The state itself is just one concrete action of the big task. 
Each state will have a specific ID to indicates which state is the current state.
*(it's preferred that the ID of the state has to be a number)*
However, as a standard way to implement this, it is proposed the following.

Let a *state*, which is divided into six steps.

1. Skip Conditions
2. Set the State
3. Business Execution
4. Error Condition
5. Before Finish
6. Persisting the State

## The Skip Conditions
These are condition when are fulfill the current state it hasn't to be performed. 
So, it's skipped to the next state.

## The Set the State
The current id of the state is set here. 
There is no restriction, any way is allowed for this action.

## The Business Execution
Here is where the current task (business logic) is executed.
There is no restriction, any way is allowed for this action.

## The Error Condition
After the previous step, it's validate if the business execution got a failure or not. 
In case of a failure, it must set some kind of error state to indicate that the current
state ended unsuccessfully.

## The Before Finish
Here is where to do final adjustments before persists the state. 
The only restriction is not to do business logic.

## The Persisting the State
Once obtained the final state, this one has to be persisted to settle the state correctly.
There is no restriction, any way is allowed for this action. However, as a good practice is
recommended to evaluate if the persisting got a failure. When that happens, it has to be
indicated as some kind of error state.

