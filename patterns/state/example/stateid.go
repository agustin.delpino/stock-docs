package state

type IdState int

const (
	ErrorStateNumber IdState = 1 << iota
	ErrorSaveStateNumber
	CreateStateNumber
	ProcessingStateNumber
	FinishStateNumber
)

func (s IdState) HasError() bool {
	return s&ErrorStateNumber != 0
}
func (s IdState) HasErrorSave() bool {
	return s&ErrorSaveStateNumber != 0
}
func (s IdState) Get() IdState {
	n := s
	if s.HasError() {
		n = n &^ ErrorStateNumber
	}
	if s.HasErrorSave() {
		n = n &^ ErrorSaveStateNumber
	}
	return n
}
