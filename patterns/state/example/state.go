package state

import (
	"log"
)

type Repo func(*log.Logger)

type State[T any] interface {
	State(T) error
}

type (
	BaseState struct {
		Logger *log.Logger
		Repo   Repo
	}
	ErrorState struct {
		BaseState
	}
	CreateSta struct {
		BaseState
	}
	ProcessingSta struct {
		BaseState
	}
	ProcessingErrorSta struct {
		BaseState
	}
	FinishSta struct {
		BaseState
	}
)

func (e *ErrorState) State(dto *ErrorDto) error {
	// handle dto.error
	e.Logger.Printf("%s\n", dto.error)
	if dto.Dto.State.HasErrorSave() {
		e.Logger.Println("the state could not be updated")
	}
	if dto.Dto.State.HasError() {
		e.Logger.Println("the state eneded with an error")
	}
	e.Logger.Printf("state: %v\n", dto.Dto.State.Get())
	return dto.error
}

func (e *CreateSta) State(dto *Dto) error {
	e.Logger.Println("performing create state")
	dto.State = CreateStateNumber
	e.Repo(e.Logger)
	return nil // e.Base.Next()
}

func (e *ProcessingSta) State(dto *Dto) error {
	e.Logger.Println("performing processing state")
	dto.State = ProcessingStateNumber
	e.Repo(e.Logger)
	return nil
}

func (e *ProcessingErrorSta) State(dto *Dto) error {
	e.Logger.Println("performing processing state")
	dto.State = ProcessingStateNumber | ErrorStateNumber
	e.Logger.Println("error at processing state")
	e.Repo(e.Logger)
	return ErrProcessingFailure
}

func (e *FinishSta) State(dto *Dto) error {
	dto.State = FinishStateNumber
	e.Repo(e.Logger)
	return nil
}

func NewErrorState() State[*ErrorDto] {
	return &ErrorState{BaseState{
		Logger: log.Default(),
		Repo: func(l *log.Logger) {
			l.Println("saved")
		},
	}}
}

func NewCreateState() State[*Dto] {
	return &CreateSta{BaseState{
		Logger: log.Default(),
		Repo: func(l *log.Logger) {
			l.Println("saved")
		},
	}}
}

func NewProcessingState() State[*Dto] {
	return &ProcessingSta{BaseState{
		Logger: log.Default(),
		Repo: func(l *log.Logger) {
			l.Println("saved")
		},
	}}
}

func NewFinishState() State[*Dto] {
	return &FinishSta{BaseState{
		Logger: log.Default(),
		Repo: func(l *log.Logger) {
			l.Println("saved")
		},
	}}
}

func NewProcessingErrorState() State[*Dto] {
	return &ProcessingErrorSta{BaseState{
		Logger: log.Default(),
		Repo: func(l *log.Logger) {
			l.Println("saved")
		},
	}}
}
