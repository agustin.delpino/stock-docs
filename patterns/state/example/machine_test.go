package state_test

import (
	"state"
	"sync"
	"testing"
)

func TestMachine_Execute_with_success(t *testing.T) {
	m := state.NewStateMachine()
	var dto state.Dto
	err := m.Execute(&dto)
	if err != nil {
		t.Fatalf("got an error at state machine: %s", err)
	}
	if dto.State != state.FinishStateNumber {
		t.Errorf("got state %d, want %d", dto.State, state.FinishStateNumber)
	}
}

func TestMachine_Execute_with_error(t *testing.T) {
	m := state.NewStateMachineWithError()
	var dto state.Dto
	err := m.Execute(&dto)
	if err == nil {
		t.Fatal("expecting an error at state machine")
	}
	if dto.State.Get() != state.ProcessingStateNumber {
		t.Errorf("got state %d, want %d", dto.State, state.ProcessingStateNumber)
	}
}

func TestMachine_Execute_with_goroutines(t *testing.T) {
	m := state.NewStateMachine()
	var wg sync.WaitGroup
	c := make(chan state.Dto, 10)

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(w *sync.WaitGroup, ch chan state.Dto, mc state.MachineState[*state.Dto], id int) {
			defer w.Done()
			var dto state.Dto
			dto.Id = id
			err := m.Execute(&dto)
			if err != nil {
				t.Errorf("got an error at state machine: %s", err)
				return
			}
			ch <- dto
		}(&wg, c, m, i)
	}

	wg.Wait()
	close(c)
	for dto := range c {
		if dto.State != state.FinishStateNumber {
			t.Errorf("got state %d, want %d at %d\n", dto.State, state.FinishStateNumber, dto.Id)
		} else {
			t.Logf("dto %d processed\n", dto.Id)
		}
	}
}
