package state

type Dto struct {
	Id    int
	State IdState
}

type ErrorDto struct {
	Dto   *Dto
	error error
}
