package state

type MachineState[T any] interface {
	Execute(T) error
}

type Machine struct {
	errH State[*ErrorDto]
	sts  map[IdState]State[*Dto]
	ord  []IdState
}

func (m *Machine) gotoState(sn IdState) int {
	for i := range m.ord {
		if m.ord[i] == sn {
			return i
		}
	}
	return -1
}

func (m *Machine) prestate(dto *Dto) int {
	switch dto.State {
	case ProcessingStateNumber | ErrorStateNumber:
		return m.gotoState(ProcessingStateNumber)
	case FinishStateNumber | ErrorStateNumber:
		return m.gotoState(FinishStateNumber)
	case ProcessingStateNumber:
		return m.gotoState(FinishStateNumber)
	case FinishStateNumber:
		return -1
	default:
		return 0
	}
}

func (m *Machine) Execute(dto *Dto) error {
	p := m.prestate(dto)
	if p == -1 {
		return nil
	}
	for ; p < len(m.ord); p++ {
		err := m.sts[m.ord[p]].State(dto)
		if err != nil {
			return m.errH.State(&ErrorDto{
				Dto:   dto,
				error: err,
			})
		}
	}

	return nil
}

func NewStateMachine() MachineState[*Dto] {
	var m Machine

	m.sts = map[IdState]State[*Dto]{
		CreateStateNumber:     NewCreateState(),
		ProcessingStateNumber: NewProcessingState(),
		FinishStateNumber:     NewFinishState(),
	}

	m.errH = NewErrorState()
	m.ord = []IdState{CreateStateNumber, ProcessingStateNumber, FinishStateNumber}

	return &m
}

func NewStateMachineWithError() MachineState[*Dto] {
	var m Machine

	m.sts = make(map[IdState]State[*Dto])
	m.errH = NewErrorState()
	m.ord = []IdState{CreateStateNumber, ProcessingStateNumber, FinishStateNumber}

	m.sts = map[IdState]State[*Dto]{
		CreateStateNumber:     NewCreateState(),
		ProcessingStateNumber: NewProcessingErrorState(),
		FinishStateNumber:     NewFinishState(),
	}

	return &m
}
