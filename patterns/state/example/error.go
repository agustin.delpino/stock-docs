package state

import "errors"

var (
	ErrProcessingFailure = errors.New("processing failure")
)
