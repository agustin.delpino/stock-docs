package state_test

import (
	"state"
	"sync"
	"testing"
)

func execute(w *sync.WaitGroup, ch chan state.Dto, mc state.MachineState[*state.Dto], id int) {
	defer w.Done()
	var dto state.Dto
	dto.Id = id
	err := mc.Execute(&dto)
	if err != nil {
		return
	}
	ch <- dto
}

func BenchmarkMachine_Execute_async(b *testing.B) {
	m := state.NewStateMachine()
	var wg sync.WaitGroup
	for i := 0; i < b.N; i++ {
		c := make(chan state.Dto, 10)
		for i := 0; i < 10; i++ {
			wg.Add(1)
			go execute(&wg, c, m, i)
		}
		wg.Wait()
		close(c)
		for dto := range c {
			if dto.State != state.FinishStateNumber {
				b.Errorf("got state %d, want %d at %d\n", dto.State, state.FinishStateNumber, dto.Id)
			}
		}
	}
}
