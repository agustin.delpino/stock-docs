---
author: Del Pino, Agustin y Ortiz, Alan
---
# Customization

The implementation of this pattern allows a lot of customization due the primitive form of the interfaces and the context passed by reference.

# Retry Policy

The retry policy can be implemented by adding a retry to the error state dto.

```plantuml
struct ErrorStateDto {
    + FooDto: FooStateDto
    + Error: error
    + Retry: bool
}
```

In the error state, given some condition, the `Retry` flag is set on. Then, in the State Machine, it will be evaluated allowing to retry the actual state. For avoid infinity retries, the State Machine has to limit them by some kind of counter.

```puml
start
:Execute State Machine;
partition "Execute" {
:Prestate;
if (prestate returns -1) then (yes)
    stop
else (no)
    :Set retry counter;
    while (has next state?)
        :Perform State;
        partition "Retry Policy" {
        if (State returns an error?) then (yes)
            :Perform Error Handler;
            if (Retry Flag is on and Retry Counter is not zero?) then (no)
                stop
            endif
            
        else (no)
            :Reset retry counter;
        endif
        }
    endwhile
    stop
endif
}
```

# Ignore Policy

The same concept of the *Retry Policy* can be used for this one. In order to ignore an error, a Ignore Flag can be added to the error state dto. 


```plantuml
struct ErrorStateDto {
    + FooDto: FooStateDto
    + Error: error
    + Retry: bool
}
```

This allows to ignore the error an continue to the next state.

```puml
start
:Execute State Machine;
partition "Execute" {
:Prestate;
if (prestate returns -1) then (yes)
    stop
else (no)
    while (has next state?)
        :Perform State;
        partition "Ignore Policy" {
        if (State returns an error?) then (yes)
            :Perform Error Handler;
            if (Ignore Flag is on) then (no)
                stop
            
            else (no)
                :Ignore Error;
            endif
        endif
        }
    endwhile
    stop
endif
}
```

# Circuit Breaker Policy

Because the State Machine is implemented as a singleton, a circuit breaker mechanism can be added for stop all the machine for some period of time.

In order to implement the CB is necessary to indicate when open the circuit. This task can be done by the error state handler or just another action *(even the actual states can trigger that)*. 

This operation has to be done before the pre-state, at every check of the circuit *(when is open)* the timer is also check to know when to close the circuit.  

```puml
start
:Execute State Machine;
partition "Execute" {
    partition "Circuit Breaker" {
        if (Is Circuit Open?) then (yes)
            if (Is timeout?) then (no)
                stop
            else (yes)
                :Close the Circuit;
            endif
        endif
    }
    :Prestate;
    if (prestate returns -1) then (yes)
        stop
    endif
    :...;
}
```

