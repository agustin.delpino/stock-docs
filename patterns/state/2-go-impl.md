---
author: Del Pino, Agustin y Ortiz, Alan
---
# Golang Implementation of the State Machine Pattern

Following the basis established, 
a golang implementation is proposed with the following goals.

- Must follow SOLID.
- The State Machine has to be a Singleton.
- The State Machine can be executed concurrently.
- The States have to be an addition to any architecture.
- The implementations have to be low infrastructure.

> *All the code can be found at the `./example` directory.* 

# The Interfaces
There two interfaces: `StateMachine` and `State`. The first one is for the State Machine and 
the another one is for the states implementations.

```plantuml
interface StateMachine<T> {
    + Execute(T): error
}
interface State<T> {
    + State(T): error
}
```

Both interfaces take a generic type that serve as context to be passed through 
all the states. 

The `Execute` method will boot up the state machine and the `State` method will 
execute a state itself.

# State Implementation

The state implementation must follow all directives and goals established  before. 
So, following those, the state implementation can be added to any type as a method.

In case of the Stock Architecture, following its rules too, the states have to be
part of the usecase, or even better, a usecase implementation can implement both 
Usecase and State Interface. And the state method will use the business logic 
of the usecase to perform the *Business Execution Step*.

```go
type FooUsecase struct {}
func (u *FooUsecase) Do(...) error {}
func (u *FooUsecase) State(...) error {
	// ...
	err := u.Do(...)
	// ...
}
```

For the ids of the states, it reasonably to use an flag kind enum to achieve the
state combination whether an error occurred. As a standard, the error states
have to be at the beginning of the enum, then the rest states. As minimum required for error states, 
there has to be two: one for any business error and another one for persisting error.

```go
type FooStates uint

const (
	FooErrorState FooStates = 1 << iota
	FooSaveErrorState
	FooCreateState
	FooProcessingState
	FooFinishedState
)
```

The type alias is recommend because it can be added helper reading method of the flags.

At the state method implementation, the rest of the steps are implemented.

```go
type FooCreateStateUsecase struct {
	fooRepo *FooRepo 
}
func (u *FooCreateStateUsecase) Do(...) (*Foo, error) {}
func (u *FooCreateStateUsecase) State(dto *FooStateDto) error {
	// 1. Skip Conditions
	if dto.SomeField == true {
		// Trace the skipping.
		return nil
    }
	
	// 2. Set the State
	dto.CurrentState = FooCreateState
	
	// 3. Business Execution
	data, err := u.Do(...)
	
	// 4. Error Condition
	if err != nil {
		dto.CurrentState = dto.CurrentState | FooErrorState
        // Trace the error.
    }
	
	// 5. Before Finish
	dto.Id = data.Id
	dto.SomeData = data.SomeData
	
	// 6. Persisting the State
	uErr := u.fooRepo.UpdateWithState(&UpdatewithStateDto{
	    Id: dto.Id,
		State: dto.CurrentState
    })
	
	if uErr != nil {
        dto.CurrentState = dto.CurrentState | FooSaveErrorState
        // Trace the error.
    }
	
	return err // forwards the error result of the usecase
}
```

# Error State Implementation
This state is special because isn't going to share the same type generic of the another state.
In this case it will receive a super-set of it which contains at least the context input of those state and 
the forwarded error. Also, as a standard, this state must handle both error and save error states.
Of course, this state must forward the given error.

```plantuml
struct ErrorStateDto {
    + FooDto: FooStateDto
    + Error: error
}
```

Next, an example implementation:

```go
type ErrorState struct {}
func (s *ErrorState) State(dto *ErrorStateDto) error {
	// Business Error
	if dto.FooDto.CurrentState.HasError() {
	    // Do something	
    } 
	// Save Error
	if dto.FooDto.CurrentState.HasSaveError() {
		// Do something
    }
	return dto.Error
}
```

# State Machine Implementation
The state machine will contain the relation between the state implementations
and the state ids. An array is provided to indicate in which order those states will be executed.
Also, the error state is separated from the others states because its special input context.

```go
type FooStateMachine struct {
	// errH is the error state for handle the errors.
	errH State[*ErrorStateDto]
	// stas is the state relation.
	stas map[FooStates]State[*FooStateDto]
	// ord is the array for provide the state order.
	ord []FooStates
}
```
So far so good.

The pre-state step is very important, because is a totally internal mechanism of the
state machine, it will be a private method. This consists in a switch that relates the 
given state and which state it supposes to perform. 

Here is the thing, as normally use, if some state ended with error it has to be retried, but
not necessarily, that depends on what is the implementation for.

Because the state flow is provided by an array, the pre-state step has to find in 
which index is the state wanted to perform, so starting the machine at that point.

Next, a pre-state example.

```go
func (m *FooStateMachine) prestate(dto *FooStateDto) int {
	switch dto.CurrentState {
	case FooProcessingState | FooErrorState:
	    return  slices.Find(m.ord, FooProcessingState)
    case FooFinishState | FooErrorState:
        return  slices.Find(m.ord, FooFinishState)
    case FooProcessingState:
        return  slices.Find(m.ord, FooFinishState)
	case FooFinishState:
		return -1
	default:
		return 0
    }
}
```

In the example above, the `Processing` and `Finish` state when they've an error are retried.
Otherwise, only the `Processing` state is redirected to the `Finish` state, in case of the latter,
there is no other next state to perform, so returns `-1`. By default, returns `0` for indicate
*"start from the beginning"*.

```puml
start
:Execute State Machine;
partition "Execute" {
:Prestate;
if (prestate returns -1) then (yes)
    stop
else (no)
    while (has next state?)
        :Perform State;
        if (State returns an error?) then (yes)
            :Perform Error Handler;
            stop
        endif
    endwhile
    stop
endif
}
```

To finish this, only rest the actual State Machine interface implementation.

```go
func (m *FooStateMachine) Execute(dto *FooStateDto) error {
	p := m.prestate(dto)
	if p == -1 {
		return nil
    }
	for ;p > len(m.ord); p++ {
	    err := m.stas[m.ord[p]].State(dto)	
		if err != nil {
		    return m.errH.State(&ErrorStateDto{
			    FooDto: dto,
				Error: err,
            })	
        } 
    }
	return nil
}
```
