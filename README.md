---
author: "agustin.delpino"
---

# Stock Docs
Official documentation of Stock Team

# Prerequisites

- VSCode
- Shell / Bash Terminal
- VSCode Markdown Extension
- VSCode PlantUML Extension

# Rules

- Use markdown for document.
- Use plantuml for diagram.
- Use lowercase for name the files and directories.
- Do not use spaces in filenames, instead use scores.
- Add a folder, then inside place the documentation.
- To add code examples, inside the doc folder, add a subfolder with the name: `examples`.
- To add images, inside the doc folder, add a subfolder with the name: `img`.
- Use shell or bash file for terminal scripts.
- To add meta-data, use metadata blocks.
- To denote order, add a number: `1-my-doc`. 
- Format the document with the `Format Document` of VSCode.
- Follow the doc template.

# Doc Template

````
---
author: "git.user"
---

# TITLE OF THE TOPIC
Small resume of the topic
````
