---
author: "agustin.delpino"
---

# Stock Programming Guideline

This document serves to explain the Programming Guideline for Stock Team.

As background knowledge you can search information in these recommend links:

- [Uncle Bob CA Blog](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)

- [Gupta Blog](https://abhinavg.net/)

- [Refactoring Guru](https://refactoring.guru/)

- [Go Tour](https://go.dev/tour/welcome/1)

- [SOLID](https://www.digitalocean.com/community/conceptual-articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design)

- [Effective GO](https://go.dev/doc/effective_go)

# Introduction

At the beginning of the Stock Team, the TL (*Cafferata Franco*) proposed to use a *CA like* architecture for golang. It's well known that thing such Architecture, Design Patterns or related are thought for OOP, making a really huge challenge for implement those things in GoLang. However, it's not impossible.

That the vast majority of enterprise pattern or architecture were created from OOP isn't a minor detail because a lot of things must to be reimplementing without anything for compare them. So, what is left is just: build a very strong fundamentals and test and error.

Based on the whole team decisions, by providing which thing are capable to be good and which not, it was determinate  this guideline.

# Scaffolding

The scaffolding was proposed by the TL and later updates follows the next structure:

---
- 📁 {project-name}: *the root of the project*
    - 📄 .gitignore
    - 📄 .gitlab-ci.yml
    - 📄 Dockerfile
    - 📄 README.md
    - 📁 ❓ cmd: *where the main entrypoint (or many) is located*
        - 📄 main.go
    - 📁 ❓ docs: *optional, api docs*
        - 📄 docs.go
        - 📄 docs.yml
    - 📁 envs: *environment variables for local executions*
        - 📄 local.env
        - 📄 proxy.env
        - 📄 test.env
    - 📁 ❓ integration-test: *where the integration tests are located*
        - 📄 integration_test.go
    - 📁 testdata: *where the common data and mocks for test are located*
        - 📄 vars.go
    - 📁 internal: *where the internal entities of the project are located*
        - 📁 server: *elemental entity for serve the application*
            - 📄 app.go: *contains the invoke and dependency injection initialization*
            - 📁 infra
                - 📄 api.go: *contains the fiber server declaration*
                - 📁 config: *where the configuration of the application are located*
                    - 📄 config.go
                    - 📄 logger.go
                - 📁 modinj: *where the injection providers are declared*
                    - 📄 appinj.go
                    - 📄 dominj.go
                    - 📄 infinj.go
        - 📁 ❓ {entity}: *an entity of the application*
            - 📁 ❓ v{n}: *ab api versioned entity*
            	 - 📁📄: *the same files that are outside*
            - 📁 ❓ service: *where entity's services are located*
                - 📄 dto.go
                - 📄 error.go
                - 📄 interface.go
                - 📄 {service-name}srv.go
            - 📁 domain: *where entity's domain is declared*
                - 📄 dto.go
                - 📄 error.go
                - 📄 model.go
                - 📄 interface.go
                - 📁 ❓ usecase: *where entity's usecases are located*
                    - 📄 {usecase-name}.go
            - 📁 infra: *where the entity's infra is declared*
                - 📁 ❓ http: *where entity's http controllers are located*
                    - 📄 dto.go
                    - 📄 {controller-name}ctrl.go
                - 📁 ❓ repo: *where entity's concrete repository implementations are located*
                    - 📄 {repo-kind}.go

---
> *The `{...} means placeholder` and ❓ means optional.*