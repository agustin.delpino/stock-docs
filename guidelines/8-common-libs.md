---
author: "agustin.delpino"
---

# Common Standard Libraries

For many many many *(many)* cases, a lot of logic/functionality is repeated in many project, that lead to break the DRY *(Don't Repeat Yourself)* Principle. Also, a same problem can be solved in many ways making difficult the maintainability and cohesion of the projects. So, the solution for both problems is a Common Standard Lib. 

A *Std Lib* isn't something new, it's from the roots of the high level programming language such C. Having an std-lib helps the developing process not only to be faster but also standard, that mean: for same problems same solution.

Now, what are the things that have to be in the common?. Use the following criteria for determinate:

- Logic that is considered *The standard way to solve a problem*.
- Duplicated code cross the project.
- Abstract logic. *(like base-classes)*.

Another good thing is that Golang can be a very handy for this purpose. That's because, the Go CLI integrates a mono-repo system called **Go Work** which can contain go modules independent one from another. The reason behind of this is for avoid bugs propagation. Let's see the next example to understand it:

**Using a go module as Common Lib**
````
[common]
  go.mod
  [string-helper]
    string.go
  [int-helper]
    int.go
````
Adding the common lib to the project by `go get common`, we are including both helpers but we only gonna use the `int-helper`. Of course while the the specific package isn't imported, it won't be compiled. But you still downloading unused packages. Also, if there some strange error on the package there's a possibility of a compilation error. *(the later version of Go compiler is very smart but we can rely that responsibility only to the compiler)*

**Using a go work as Common Lib**
````
[common]
  go.work
  [string-helper]
    string.go
    go.mod
  [int-helper]
    int.go
    go.mod
````

Now, each package become a module, and the Common module become a GoWork. By this way, the modules can be added one by one and are totally independent one from another. The installation of this modules can be done by `go get common/string-helper` or/and `go get common/int-helper`.

## API Versioning Criteria

Become a common/standard logic has a huge responsibility, not only for being the standard way to solve a certain problem but also because that logic is replicated in many other project. Because of that, the ***Signature*** of the symbols must not change. ***Once made the firm never must change***. If it did it, the projects where the module is used will be blow up in production because they will install the lasted version of the module then, if the firm changed, the compiler will throw an error. 

But, there's a solution for this problem: *Internal API Versioning*. This solves the signature problem, if we versioned the module by the changes of signature it will remain the backward compatibility and co-existence of old and new signature in the same project.

Let's take a look to an example.

````go
func Sum(a int, b int) int {
  return a + b
}

func calc(a int, b int) {
  println(Sum(a, b))
}
````

The `Sum` function takes two `int` and returns an `int`. 

Then, the function `Sum` changes its signature for accept float values.

````go
func Sum(a float64, b float64) float {
  return a + b
}

func calc(a int, b int) {
  println(Sum(a, b))
}
````

This change leads to a compilation error, because it can be use int as float. So, we are obliged to do not change the signature, even if we have a requirement that implies that change. The worst solution is to redeclare locally the function (**YOU DON'T MUST TO DO THAT**).


Instead of that, use the following criteria when creating the common-modules.

````
[common]
  go.work

  [my-module]
    go.mod

    [v1]
      file.go

    [v2]
      file.go
````

The scaffolding above shows how the module are compound. Having a sub-folder which is the last version of the signature.
The module scaffolding must follow the next rules:

- The name of the folder must be `vN` where `N` is a natural number.
- The name of the files cross version must be the same.
- The name of the symbols must remain the same.

## Documenting the Common Lib

Another important topic is the Code Documentation *(later discussed in particularity)*. For this kind of libs there are extra rules aside of **Documentation Conventions**.

- The docs must contain usage examples. To do that, use this format:
  - The *Usage*, describe the way that logic is expected to be used.
  ````go
  // Sum ...
  /*
  # Usage

    r := Sum(12, 10)
  */
  func Sum(a int, b int) int {
    return a + b
  }
  ````

- The docs must clarify than describe.
  
  Here, the doc describes what the function do. 
  ````go
  // Sum returns the sum of two numbers. <- DO NOT
  ````

  Here, the doc clarify what the function do. Because is using the function arguments to achieve that.
  ````go
  // Sum returns the sum of a and b. <- DO THIS INSTEAD
  ````

- In case error are returned, the docs must contain an error section. To do that, use this format:
  ````go
  // Foo ...
  /*
  # Errors
    - ErrVarName: when something happens.
  */
  ````