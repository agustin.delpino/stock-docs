---
author: "agustin.delpino"
---

# Standard Logging

Based on the [Tracing Conventions](/logistica/stock/stock-standards/guidelines/code-conventions#tracing-conventions), and the [Common Logger](https://gitlab.com/fravega-it/logistics/stock/stock-common/-/tree/master/logger) from *Stock Common*. Here is the standard way for logging.

# Understanding the Logger
The logger is based on the **Prototype Pattern**, so each logger instance is a clone of a master/original logger. This original logger is created at the application boot-up and each entity that need the logger will clone it.

````puml
rectangle "App" {
  [Logger]<--[Entity] : Clones
  [Entity]-->[Entity] : Stores Logger's Clone
}
````

Under the hood, all logger's clones aims to the same place to log, because they share the same *Output*. 
> The logger is concurrent safe, so no worries for using in go-routines.

Also, this logger implements the "leveled logging" and "strong typing field".

***THE LOGGER ONLY LOGS IN JSON FORMAT***

## Why is the logger a prototype?
Short awnser: for reuse information. By being a prototype, each logger instance is a copy of the original one, so the original settings remains in its clones, and because those clones can be clonned too, each individual configuration can be copied, and so on. However the that configuration only affects to the current instance and its clones, not to the *"parent instance"*, which is greate for work with concurrent operations *(go-rountines)*.

````puml
start
partition "boot-up" {
	:Create the Logger;
  partition "new-logger" {
    :Configure the Original Logger;
  }
	:Create the Entity;
  partition "new-entity" {
    :Clones the Logger for the current entity instance;
    :Set individual configuration;
  }
}
:...;
partition "Any action of Entity" {
	 fork
    :Clones the Logger for the current action;
    :Set individual configuration;
    :...;
  fork again
    :Clones the Logger for the current action;
    :Set individual configuration;
    :...;
  fork again
    :Clones the Logger for the current action;
    :Set individual configuration;
    :...;
  end fork
}
end
````

Reusing information is great for code readability and performance, because the information is only given once, avoiding to give it at each time the logger is used for tracing something.

# Logger API

The current API *(Logger v1)* supports the following

- Clone
- Clone by common tracing
- Fields addition as metadata
- Logging methods
- Logging methods with volatile metadata

> You can see the interfer [here](https://gitlab.com/fravega-it/logistics/stock/stock-common/-/blob/master/logger/v1/logger.go#L49)

**Clone**

Allows to clone the current logger instance, it copies the configuration of its parent logger.

**Clone by common tracing**

The same as *Clone* but admits a *`Traceable`* interface, which provides some kind of **id** as string for relate many logs.

**Fields addition as metadata**

Allows to add fields into the logger instance. This is the actual *individual configuration* mentioned before.

**Logging methods**

Allows to log the message in different levels. Those are:

- Panic
- Event
- Info
- Warn
- Error
- Debug

**Logging methods with volatile metadata**

Allows to log the message in different levels and also admits fields to add only in the current message (the provided fields won't be stored in the logger instance).

# Strong Typed Fields

The logger's fields are used for log metadata along with the actual log message. Theses fields are strong typed, so if the metadata is a `string` the way to create the field is by using the specific function for create String Fields. And then, so on for the rest of the types.

> Go [here](https://gitlab.com/fravega-it/logistics/stock/stock-common/-/blob/master/logger/v1/field.go) for see all the field types available.


# Usage conventions

There are a few conventions for using this logger, first of all is the strict follow of all ***Tracing Conventions***.

## Shared Fields
The shared fields are the fields which are stored in the current instance of the logger and are shared for all log messages.

These fields must be exclusive for logs correlation like: IDs, URLs, status codes, scopes *(like the current method and instance)*, etc.

It's totally prohibit to use these fields for store any other kind of metadata like: data structs *(arrays, objects, maps, etc)* and error exceptions.

## Volatile Fields

The volatile fields are the fields which are added to one single log message. These fields are allowed to include any kind of data. All prohibit metadata kind in the Shared Field are allowed.