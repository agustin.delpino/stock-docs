---
author: "agustin.delpino"
---

# Testing
Testing the code is one of the most important parts of the development, because adding test contributes to the quality assurance of the code.

All public functions/methods must have many unit-tests as needed for cover at least the 80% of the code. But the 100% is always required.

## Standard Libs for Testing
- `testing`, native lib from golang.
- `testify`, a java asserting approach.
- `mockery`, autogenerate mocks for interfaces.

## Testing Scaffolding

Align with the **Standard Scaffolding**, the test files must be placed at the same level of the current file which is being tested.

The test files must have the same name of their parent file and `_test.go` as suffix.

Also, the package of the test files must be a sub-test-package, a golang feature that helps to encapsulate the test and allows to share code by importing with other sub-test-packages.

The private symbols that have to be tested, can be exported with a `export_test.go` *(one per package)*.

** An example of scaffolding**

````
[foo]
  foo.go
  export_test.go
  foo_test.go
[testdata]
  barmock.go
  vars.go
````

**An example of a test file**
````go
package foo_test

import "module/of/the/project/testdata"

func setup(t *testing.T) (*Foo, *testdata.BarMock){
  m := testdata.NewBarMock(t)
  return NewFoo(m), m
}
````

## Testing Principles

The pillars that holds the testing are:
- ***G:W:T***
- ***w/ distinct Criteria***
- ***AAA Pattern***
- ***Setup Pattern***
- ***Asserting Criteria***
- ***Mocks Usage Criteria***
- ***Solo Coverage Criteria***

> *See more   
> [Give-When-Then](https://en.wikipedia.org/wiki/Given-When-Then)  
> [AAA Pattern](https://java-design-patterns.com/patterns/arrange-act-assert/)*

Those principles are formed by common design test pattern and self-crafted criteria.

## Given-When-Then

This principle remains to the test documentation. The test docs are very important because are the first approach of what it's going to test and how. Additionally provides an specification of the test that describes what the test does.

As general aspect, the **Documentation Conventions** must be follow. Also, for this case a comment block must be used with the next format.

````go
/*
Given: something.
When:  performs... . 
Then:  returns... .
*/
````

**Given**

Indicates what thing or things serve as input. Commonly is the function/method arguments.

For example, let a `Sum(a int, b int) int`. The *Given* doc and some of its variants will be:

````go
// Given: two positive numbers.

// Given: a positive number and a negative number.

// Given: a zero and positive number.
````

**When**

Indicates what action is performed in the test. **Do not use the name of the function**, instead use some verb that easy describes the action.

For example, let a `RetrieveById(id string)`. The *When* doc and some of its variants will be:

````go
// When: retrieves the doc.

// When: retrieves the doc but some failure occurs.

// When: retrieves the doc and some other thing happens.
````

**Then**

Indicates the expected result of the operation. Commonly is the output of the function/method.

For example, using the same function from the *When*:

````go
// Then: returns the doc and no error.

// Then: returns no doc and some error.

// Then: returns the doc with the some field modified and no error.
````

**IMPORTANT**

The test docs must be human readable, also can link the symbols by using its names *(something really util for the errors)*.

## w/ Distinct Criteria

This criteria is used for naming the tests. 

As its name says, the test names must be end *with some distinct*, providing some information that distinct the current test from the another ones.

The format is: `TestSomething_with_some_distinct`

The reason of this is for make the test more visible at the naked eye in context where few information is provided. A real case scenario is the Pipeline of the CI/CD where the console output can be messy and very non-understandable. 

Giving this format makes the test name unique and easy to recognize.

## AAA Pattern

This pattern is used for declared the test. 

The *AAA* stands for *Arrange, Act, Assert*. Basically, that must be how the test is compound.

For example.

````go
func TestSomething_with_some_distinct(t *testing.T) {
  // arrange: call setups, prepare mocks, etc.
  
  s, m := setup(t)
  
  m.On("Upper", mockery.Anything).Return("TEST", nil)

  // act: executes the logic to test and post processing if it needed.

  o, oErr := m.Do("test")

  // assert: verify the result of the test.
  assert.NoError(t, oErr)
  assert.Equal(t, "TEST", oErr)
}
````

## Setup Pattern

This test pattern is a classic one. In some other langs is called `setupBefore` or just `before`.

The pattern consists in declare one or many setups for instance the current thing to test and the mocks to use along the testing.

For example.

````go
func setup(t *testing.T) (*StringService, *StringerMock) {
  m := NewStringerMock(t)
  return NewStringService(m), m
}
````

As it looks, with the setup, the initialization logic is centralized and encapsulated in one place making the test only have the concrete things that have to be tested. Also, use the constructor function, which contributes to the test coverage.

In case you need more than one mock, you can declare a local struct that contains all the mocks.

````go
type mocks {
  bar *testdata.BarMock
  fii *testdata.FiiMock
}

// ...

func setup(t *testing.T) (/*...*/, *mocks) {
  m := &mocks{
    bar: testdata.NewBarMock(t),
    fii: testdata.NewFiiMock(t),
  }
  return /*...*/, m
}

````

> The setups are called at `Arrange` section.

## Asserting Criteria

This criteria is for use an idiomatic assertion at testing. Thanks to [Testify](https://github.com/stretchr/testify), a lot of specific asserts can be made. Here are some of them:

- Use `assert.Error`, for assert when a error must be returned.
- Use `assert.NoError`, for assert when no error must be returned.
- Use `assert.ErrorIs`, for assert an specific error.
- Use `assert.NotNil`, for assert when a pointer must be returned.
- Use `assert.Nil`, for assert when no pointer must be returned.
- Use `assert.Equal`, for assert a value.
- Use `assert.Len`, for assert a length.
- Use `assert.Zero`, for assert a numeric value against 0.
- Use `assert.True`, for assert a true value.
- Use `assert.False`, for assert a false value.

## Mock Usage Criteria

This criteria is based on [Mockery](https://vektra.github.io/mockery/latest/). With this lib the interface mocks is autogenerated.

The way to use the mocks is the following:

1. Once you got all interfaces declared, execute this command at root of the project for create the mocks.
  ````shell
  mockery --output ./testdata/ --outpkg "testdata" --all
  ````

2. Instance them in the setup function at a test.
3. At the `arrange` section, use me method `On` for mock the function call. Then use the `Return` method for return the mocked values.

  ````go
  m.On("MethodName", mockery.Anything).Return("Values")
  ````

An important thing to know is when use `mockery.Anything` and `mockery.MatchedBy`. Follow the next criteria to determinate.

- Use `mockery.Anything` when:
  - The argument values doesn't matter for the return values. E.g.: returning errors.
  - The argument values were forwarded with any conversion.
- Use `mockery.MatchedBy` when:
  - The argument values were converted into other thing. E.g.: DTO mapping.
  - The argument values are necessary for execute one specific mock. E.g.: concurrency processes.

## Solo Coverage Criteria

When testing, an interesting question come across: Am I testing right?. That question isn't a minor thing, because poor testing can blind the vision and make believe that the code was fully tested.

However, if you think few tests are equal to poor testing, well, YOU WRONG.

May you have ten thousand of test and even have a poor testing. Why is that?, that's because the **Coverage**.

One of the most util tools used for testing is the **Coverage** which is a coficient as result of the quotient between *executed code lines* and *existing code lines*. Usually that coficient is multiplied by a hundred for convert it into a percent.

The way to calculate the coverage is the following:

$$
c = executed \ lines \\
l = existing \ lines \\
C = \frac{c}{l} \times 100\%
$$

> From now the unit $ut$ means `unit-test` and $l$ means `lines`.

So, let $l$ with $500 l$ as a value and let $c$ with $300$ as a value.
All of that means that the code on testing has $500$ lines and the executed test only *"pass through over"* $300$ lines, remaining $200$ lines that were not tested. And the $C$ result will be: $300/500 = 0.6$ or $60\%$ of **Coverage**.


Perhaps, other people can think 60% of coverage is a good one, but for the Stock Team is nothing, the minimum percent accepted is 80% (with a good explanation of why didn't reach the 100%).

Now, using the previous example, something odd can happened. Let's say that $60\%$ of coverage is made by a ten unit-test. The math says that the mean rate of coverage variation is $60\% / 10 = 6\%$. The primitive logic says, one test represents a $6\%$ of coverage, knowing the remaining coverage is $40\%$, making some easy math: $40 / 6 = 7$, the result that outputs is there are still $7$ test left to reach the $100\%$.

> To corroborate, $10ut + 7ut = 17ut \rArr 17ut*6\% = 102\%$, surpassing the $100\%$. This is telling, the model isn't very accurate.

But, because there is always a but. This mathematical model is wrong, or not very accurate. That's because it's using the **Mean Rate**, granting to each test the same amount of coverage, something that probably isn't true. Let's do some relation to understand it well.

*In this case, values are going to be used for clarify the algebra.*
$$
\begin{rcases}
l = 600l \\
c = 300l \\ 
\end{rcases}\rArr
C = \frac{c}{l} = 0.5 \\
$$
$$
T = \{a,b,c,d\}\\
t = \#T = 4ut \\
$$

The above model is the fundamental piece for start. There is $l$ and $c$ that contains lines, $C$ as the coverage coficient and $T$ the set of unit-test, also there is $t$ which is the cardinal of $T$.

If the tests are executed one by one, the output result will be a set of coverage values for each unit-test.

$$
U(x): \N \rarr T = "coverage \ of \ x" \\
$$
$$
W = Img \ U
$$

The $W$ set has the coverage of each test individually. So, from know both $W$ and $T$ have a functional relationship by $U(x)$.

The $W$ set has less elements than $T$, that's because the $T_c$ and $T_d$ has the exact same coverage. This scenario is telling both test cover the exact same amount of lines. However, same amount isn't same lines. But here is the deal, $0.1 + 0.2 + 0.3 = 0.6 \rArr 0.6 \neq 0.5$. Why is this? Well, it because the unit-test can pass for exact same lines. So, it seem the unit-test cannot see total independent one of another to measure the coverage. To know how much is that amount just subtract the result with the actual coverage $\sum{W} - C$.

The final piece to this puzzle is the test accumulation: one test, two tests, three test instead of first test, second test, third test.

Now, let's define $A$ to follow that new model and redefine the $U(x)$ .

$$
P' \subset P(T) = \{\emptyset, \{a\}, \{a,b\}, \{a,b,c\}, \{a,b,c,d\}\} \\
$$
$$
A = \{x: 0 \le x \le \#T\} 
$$
$$
U(x): A \rarr P' = "the \ coverage \ of \ u \in P' / \#u = x" \\
$$
$$
W = Img \ U
$$

Great, with this new model the coverage is accumulated thanks to execute more than one test.

Here is the thing, by this model it is proved that executing the `{a,b,c}` or `{a,b,c,d}` subset of tests is completely the same thing. Even more deep, the intersection of both result into a new subset that contains all redundant tests. 

The redundant tests are those that do not contribute anything to increase the coverage. Meaning a waste of work, and here is why, even making a lot of test isn't equal to a better or higher coverage.

Moreover, measuring the variation of coverage it can proves easy the totally non contribution.

From $3$ to $4$:
$$ 
C = U(4)-U(5) \rArr C = 5 - 5 \rArr C = 0
$$

From this model a lot of other things can be obtain by calculus. Take the time to discover them!.

**Conclusion**

As a conclusion of this criteria, the most important thing is to increase the coverage, the test doesn't contribute to that are just a waste of code and work. 

**So, DEVs, focus on the coverage!** 