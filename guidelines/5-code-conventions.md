---
author: "agustin.delpino"
---

# Code Conventions

The conventions are the rules for coding and code reviewing. These conventions must be follow to achieve the correctness of coding.

## Naming Convention

- All general symbols must be clarify what they are. It has to be very explicit.
  - DO NOT use short names.
  - DO NOT use abbreviations.
  - DO NOT use numbers.
  - DO NOT use underscores.
- The error variables must have the `Err` prefix. 
  
  E.g.: `ErrNilPointer`.
  - DO NOT use the word "error" in the errors. If you need it, use instead the word "failure".

- The local variables inside a scope (also the argument of a function) must be short.
  
  E.g.: Response -> `rs`, Result -> `ret`, Item -> `i`, `itm`.

  - DO NOT use large names.
  - DO NOT use underscore (only for ignore a variable).

- The catching error variables must has the name of the catching result variable as prefix.

  E.g.: `rs, rsErr = http.Get()`

  - Only when there is no other error variable, it can use the `err` default name.

- The name of function inside a package cannot start with the name of the package.
- The name of user define method must start with a a verb.
  
  E.g.: `GetSome()`, `MapTo()`, `Retrieve()`

- The receiver instance names must be only one letter and be the first one at the name of receiver.
  ````go
  type Enum uint8

  func (e Enum) Value() string {}
  ````
- The constructor functions must start with `New` follow by the type name.

  E.g.: `NewItemMongRepository`, `NewItemPostgresRepository`, `NewItemRestRepository`.

## Architecture Conventions

The following rules are added to the exiting conventions.

- The controllers must have `Controller` as suffix. 
  
  E.g.: `CreateItemController`.

- The service implementations and interfaces must have `Service` as suffix: 

  E.g.: `CreateItemService`.

- The usecase implementations and interfaces must have `Usecase` as suffix: 

  E.g.: `CreateItemUsecase`.

- The repository implementations and interfaces must have `Repository` as suffix. 
  E.g.: `ItemRepository`.

  - For the implementations add the implementation kind.

    E.g.: `ItemMongRepository`, `ItemPostgresRepository`, `ItemRestRepository`.

- The receiver instance names of the `Controller`, `Service`, `Usecase` and `Repository` must be `c`, `s`, `u` and `r`, respectively.
- The services, usecases and repository must implement an interface.
- The interface's implementations must be privates.
- The interface of service and usecase only have the `Do` method.
- The inner entities of each layer can only interact with an interface by a DTO or Model.
- The repository can have all CRUD and user defined methods.
- The constructors of an implementation must always returns the interface.
- The `dto` must be the name of the argument for the implementation of `Do` method.
- The errors must be static. **It's totally prohibited create them at functions/methods**.
- The private symbols can have a more lax docs or even haven't one. 
- The controller must be documented with this format: `CreateItemController handles the Item Creation`
- The services and usecases are implementations, so they follow the *Implementation Docs Rule*: `CreateItemService implements ...`

**Interface usage example**
````go
// Service provides ...
type Service interface {
  // Do returns an error ...
  Do(*ServiceDTO) error
}
// Usecase provides ...
type Usecase interface {
  // Do returns an error ...
  Do(*UsecaseDTO) error
}
// Repository provides ...
type Repository interface {
  // Create returns an error ...
  Create(*Model) error
}

// fooService implements Service.
type fooService struct {
  us Usecase    
}
func (s *fooService) Do(dto *ServiceDTO){}

// fooUsecase implements Usecase.
type fooUsecase struct {
  repo Repository 
}
func (s *fooUsecase) Do(dto *UsecaseDTO){}

// fooRestRepository implements Repository.
type fooRestRepository struct {
  h HttpClient   
}
func (s *fooRestRepository) Create(m *Model){}
````


## Declaration Conventions

- Do not use return variables.

  ````go
  func Exists() (ok bool) // DO NOT.
  ````
- The errors always must be at the right.

  ````go
  func Foo() (error, Bar) // DO NOT.
  ````
- The models or DTOs which aren't instance at any point, can have inline declarations.
- Always group the const and variables.
- The declaration order must follow the next format:
  1. Vars, Const, Type Alias.
  2. Interfaces and Structs.
  3. Public Methods.
  4. Private Methods.
  5. Public functions.
  6. Private functions.

## Flow Conventions

- Every time that is possible, the early `return` must be for an error case.

  ````go
  func Retrieve(id string) (*Model, error) {
    m, mErr := db.Get(id)

    if mErr != nil {
      // early return
      return nil, mErr
    }
    // ...
  }
  ````

- Every time that is possible, the final `return` must be for the default case.

  ````go
  func Retrieve(id string) (*Model, error) {
    m, mErr := db.Get(id)

    if mErr != nil {
      return nil, mErr
    }
    
    // default return
    return m, nil
  }
  ````

- Keep default logic at the main scope.

  ````go
  if ok {  // DO NOT
    // default logic
  } else {
    // not ok
  }
  ````

  ````go
  if !ok {  // DO NOT
    // not ok 
  }

  // default logic
  ````

- Avoid inline scopes at statements.
  - Using inline scopes breaks the *Main Scope* rule.
  ````go
  if v, ok := m[id]; ok {  } // DO NOT
  ````
  
  ````go
  v, ok := m[id] // DO THIS
  if !ok {}
  ````

- The error description must be in lower case.
  ````go
  var ErrSomeFailure = errors.New("some failure")
  ````

## Tracing Conventions

- Use only the common logger.
- Every instance and every method must instance its own logger.
- Use a cross-trace-id for relate all log across a transaction.
- Log at the start and end of a function/method for trace the execution.
- Use the related log level for log out the messages.
  - `Info`, for describing messages.
  - `Warn`, for alternative flows or omitted errors.
  - `Errors`, for error messages.
  - `Debug`, for debug messages.
- Use only static and const message for logging.
  - Always lower case.
- Use the logger's fields to add metadata.
  - Always lower case and separate the words by a score.
- Use relation group for related fields.
- Use the standard fields for logging metadata.
  - `logger`, indicates the current instance.
    - Must contain the name of the instance's object.
  - `sub-logger`, indicates the current scope *(commonly used by methods)*.
    - Must contain the name of the current scope *(method's name)*.
  - `dto`, contains to the input DTO.
  - `trace-id`, contains to a global id for the entire operation.
  - `id`, contains a local id.
  - `url`, contains an url.
  - `error-`, refers to something related to error.
    - `error-exception`, contains the caught exception *(Go Error)*.
  - `http-`, refers to something related to HTTP.
    - `http-url`, contains the url to perform the request.
    - `http-status`, contains the http status code.
    - `http-req`, contains information about the request.
    - `http-res`, contains information about the response.
    - `http-header`, contains information about a http header.
    - `http-body`, contains the http body.
  - `soap-`, refers to something related to SOAP.
    - `soap-url`, contains the web service url.
    - `soap-method`, contains the soap method to call.
    - `soap-code`, contains the soap method's return operation code.
  - `gql-`, refers to something related to GQL.
    - `gql-url`, contains the url of the GQL service.
    - `gql-query`, contains the raw query.
    - `gql-op`, contains the operation name.

** Example **
````go

type Foo struct {
  logger cmlog.Logger
}

func (f *Foo) Bar(dto *InputDto) (*OutputDto, error) {
  l := f.logger.Clone().Fields(
    cmlog.String("sub-logger", "Bar"),          // add the sub-logger field with the name of the method.
    cmlog.String("trace-id", dto.GetTraceId())  // add the trace-id field.
    cmlog.Struct("dto", dto)                    // add the dto field
  ) 

  l.Info("retrieving user") // start message

  var (
    o *OutputDto, 
    oErr error 
  )

  if dto.id == "" {
    l.Warn("no id was provided, it will use the name and document id") // warning message for alternative flow.
    o, oErr = DB.GetByNameAndDocID(dto.Name, dto.DocID)
  } else {
    o, oErr := DB.Get(dto.id)
  }

  if oErr != nil {
    l.Fields(cmlog.Error("error-exception", oErr)).Error("") // add the error-exception field and log an error message.
    return nil, oErr
  }

  l.Info("user was retrieved") // end message
  return o, nil
}

func NewFoo(l cmlog.Logger) *Foo {
  return &Foo{
    logger: l.Clone().Fields(cmlog.String("logger", "Foo")) // add the logger field with the name of the struct.
  }
}
````

> The score is used for separate word because Elastics-Search doesn't generate a subfield.

## Documentation Conventions

- All docs must be in **ENGLISH**.
- Any docs starts with the name of the symbol.
  ````go
  // Foo ...
  var Foo string
  ````
- Enhance the documentation with the go-doc syntax.
- All docs must end with a period.
- Use verbs in third singular person to describe the symbol.
  - Use `contains`, for structs such as Models and DTOs.
  - Use `is`, for simple variables and fields.
  - Use `is returned when`, for error variables.
  - Use `provides`, for interfaces.
  - Use `returns`, for method and functions.
  - Use `is an enum for`, for enum types.
  - Use `implements` follow by the interface name for any interface implementation.
  - Use `extends` follow by the type for any type compound.
  - Use `returns a new instance of` follow by the type for any constructor function.
  - Use `represents` for indicate a symbol is an abstraction of something.
- Document the return domain errors as a list at function/method using this format:
  ````go
  // # Errors
  //  - ErrSomeFailure.
  ````

**Documentation Example**
````go
// DefVal is a default value.
var DefVal = "def-val"

// ErrNotNaturalNumber is returned when a math operation result into a non-natural number.
var ErrNotNaturalNumber = errors.New("the resulted number is not natural")

// State is an enum for states.
type State uint8

const (
  // Init represents the initial state.
  Init State = iota
  // Processing represents the processing state.
  Processing
  // Finished represents the finished state.
  Finished
)

// Request represents an http request.
type Request struct {
  // Url is the url to use for perform the request.
  Url string
  // Body is the request payload.
  Body []byte
}

// Response contains the http response information.
type Response struct {
  // Status is the http status code.
  Status int
  // Body is the response's body.
  Body []byte
}

// HttpClient provides a client for perform http requests.
type HttpClient interface {
  // Do returns the http response by performing the Request. In case of failure, returns an error.
  Do(*Request) (*Response, error)
}

// Client implements HttpClient.
type client struct {
  h *HttpTransport
}

func (c *client) Do(r *Request) (*Response, error) {}


// Sum returns the sum of a and b. In case the number is less or equal than zero, returns an error.
// - ErrNotNatural
func Sum(a int, b int) (int, error) {
  r := a + b
  if r <= 0 {
    return 0, ErrNotNatural
  }
  return r, nil
}

// NewClient returns a new instance implementation of HttpClient.
func NewClient() HttpClient {
  return &client{h:DefaultTransport()}
}
````