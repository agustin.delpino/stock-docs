---
author: "agustin.delpino"
---

# Starting a New Project

When a new development is started there are many consideration to take care:

1. Use the standard scaffolding by the `.scaf` or a the standard gitlab-template.
2. Once created the go.mod, use this module name: `gitlab.com/fravega-it/logistics/stock`.
3. Pair the docker image golang version with the compiler version. **Both version must be the same**.
````go
// go.mod
module gitlab.com/fravega-it/logistics/stock/{project-name}

go 1.20 // <- Compiler version
````
````Docker
# Dockerfile
FROM registry.gitlab.com/fravega-it/arquitectura/godocker:1.20.2 AS build # <- Docker Image version
````
4. Use the standard gitlab-ci *(always verify)*.
````yml
include: 'https://server-pipeline.management.fravega.com/master/agile-ci-cd.yaml'
````

With all these consideration done, you are able to start coding.

# Folder & Files

Inside a project, the names of the folders and files must follow the next rules:

- The folder names must be short and describe the concrete subject of the entity.
- The file names must be short and even abbreviated.
  - The configuration files will take only one name: `config.go` *(for application environment config)*, `http.go` *(for http client)*, `logger.go` *(for logger)*.
  - The controller files will take an abbreviation from the its actual name follow by the suffix `ctrl.go`: `crtitmctrl.go` *(from CreateItemController)*, `upditmctrl.go` *(from UpdateItemController)*.
  - The services files will take an abbreviation from the its actual name follow by the suffix `srv.go`: `obtitmsrv.go` *(from ObtainItemService)*, `schitmsrv.go` *(from SearchItemService)*.
  - The usecases files will take an abbreviation from the its actual name follow by the suffix `us.go`: `pcsitmus.go` *(from ProcessItemUsecase)*, `clcqtyus.go` *(from CalculateQuantityUsecase)*.
  - The repositories files will take the implementation as name: `rest.go` *(from a rest implementation)*, `mongo.go` *(from a mongo implementation)*.
- For entities that have a high probaility of mutate in the future (provoking breaking changes and/or backward compability), it's mandatory to use the api versioning entity scaffolding.

The following rules are the ***DO NOT***:

- Plural names such as: `errors.go`, `dtos.go`, `models.go`, etc *(always use singular)*.
- Word Casing such as: `itemSearcher`, `item_searcher`, `ItemSearcher`, etc. 
  - *Both folders and files must have lower case names.*
  - *Folder names can have '-' in their names*.
- Entity's files at root of the entity. *(the files must be inside of their respective section/layer)*

## Why this criteria?

This criteria was taken thinking of how golang modules and packages works. The unit of golang is the file but it always must be contained inside a package, every file that shares the same package it will treated as one, that why the those file can share golang symbols *(functions, vars, etc)*. So, the real important thing is the package, that's why the names of the file are so compressed.

In other hand, many other languages treat the file names with greater importance, for example: being actual part of the *import path*, the name must be match with the class inside of it, etc. For golang all of that doesn't matter, only the *Module Name* and the *Package Name*.

The suffixes for entity's files have the propose see which *"part"* of the program got a failure at debugging/testing the program. For example, if some error occurs at the Controller, it can quickly see that file is a `...ctrl.go`, so you known in which layer occurs the problem. Also, this distinguished form of naming helps to know which files belong to the current project and which not.

The api versioning avoid backward incompatiblity and mantains legacy version alive while the new version await to be fully implemented.