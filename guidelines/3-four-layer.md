---
author: "agustin.delpino"
---

# The Four Layers Principle

As first principle, it has the division of the logic flow which has four layer: **Controller**, **Service**, **Usecase** and **Repository**. Just as it looks, the execution flow follows that order.

````puml
(Clients) -> (Controller)
(Controller) -> (Service)
(Service) -> (Usecase)
(Usecase) -> (Repository)
(Repository) -> (External Services)
(Controller) <- (Service)
(Service) <- (Usecase)
(Usecase) <- (Repository)
````

Both *Controller* and *Repository* belong to the **Infra Section**, while the *Service* and *Usecase* form part of the **Business Section**.

At the **Infra Section** are located the two edges of the application: Client Side (Web, Rest, Topics, etc) and External Service Side (Microservice, WebServices, DBs, etc).

In the middle is the **Business Section** where all business rules are located. The majority of the logic is inside of the usecase, being the service just a I/O port for the controller.

## The Fundament of the Layer Segregation

Segregating the logic flow in this four layer is based on the **Decoupling Proposal** of *CA*, which delegates to each layer a certain responsibility without knowing the responsibility of the another ones and the way those layers interact is by **DTOs** *(Data Transfer Object)*.

This is great, making the layer independent one from another it became an a very flexible and scalable application. Something that changes at repository level doesn't affect to the service level, as an example.

Let's take a look to an example of it.

Let a service and an usecase where both have a output struct that can be related 1:1.


````puml
hide empty members
skinparam linetype polyline


class Service {
    +Do(dto: InputServiceDto): OutputServiceDto
}

class Usecase {
    +Do(dto: InputUsecaseDto): Model
}

struct Model {
  FieldA: int
  FieldB: int
}

struct OutputServiceDto {
  FieldA: string
  FieldB: int
}

Service ---> Usecase : Call Usecase
OutputServiceDto - Service
Model - Usecase
OutputServiceDto <-- Model: 1 : 1
````

Then, change usecase's output by replacing some field with another. Breaking the original output relation.

````puml
hide empty members
skinparam linetype polyline

struct Model {
  FieldB: int
  FieldZ: float
  FieldX: bool
}

struct OutputServiceDto {
    FieldA: string
    FieldB: int
}
````

This is not a problem because the service can make a new relation without affecting other layers.

````puml
hide empty members
skinparam linetype polyline


class Service {
    +Do(dto: InputServiceDto): OutputServiceDto
}

class Usecase {
    +Do(dto: InputUsecaseDto): Model
}

struct Model {
  FieldB: int
  FieldZ: float
  FieldX: bool
}

struct OutputServiceDto {
    FieldA: string
    FieldB: int
}

Service ---> Usecase : Call Usecase
OutputServiceDto - Service
Model - Usecase
OutputServiceDto <-- Model: o.FieldA = m.FieldX + m.FieldZ
````

Cases like that are very often when the applications grows or their external dependencies has breaking changes.

