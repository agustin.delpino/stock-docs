---
author: "agustin.delpino"
---

# Third Party Libraries

The use of third party libs has a lot of importance because a bad choosing of them can lead to terrible problems, for example:

- Deprecations.
- Security Exploits.
- Breaking Changes.
- Non-long-term support.
- Hidden bugs.
- Low or none new updates.

For avoid *"ugly libs"*, it's recommended this criteria:

- The owners of the lib is a company/organization/big community.
- The lib has a good docs.
- The lib is used by many people.

![image.png](img/image.png)

- The lib has sponsors or is proved that companies or big project are using it.
- The repository has a big amount of starts, watching and forks.

![image-1.png](img/image-1.png)

- The last release is relative new and there is good amount of deployed releases.

![image-2.png](img/image-2.png)

- The compiler version is relative new or at least one down from the LTS.  

![image-3.png](img/image-3.png)

- The lib proves its efficient with some kind of benchmark comparison.

![image-4.png](img/image-4.png)

- The lib has few reported issues.

![image-5.png](img/image-5.png)

- The releases are well documented.

![image-6.png](img/image-6.png)

## Standard Third Party Libs List

The following list shows the standard third party libs which are recommend to use, in one hand because meet the criteria and in other hand because they serve the architecture.

- https://github.com/uber-go/fx](img/https://github.com/uber-go/fx)
  - Dependency Injector by Uber.
- https://github.com/gofiber/fiberr](img/https://github.com/gofiber/fiber)
  - An "express like" infrastructure for *fasthttp*.
- https://github.com/kelseyhightowernfig](img/https://github.com/kelseyhightower/envconfig)
  - By now stay but it will be deprecate in the future (doesn't update since May 24, 2019)
- https://github.com/hashicorple Http](img/https://github.com/hashicorp/go-retryablehttp)
  - It a wrapper around the native http client of golang.
- https://github.com/json-iterator/goer](img/https://github.com/json-iterator/go)
  - The fastest json parser and way better than the native one.